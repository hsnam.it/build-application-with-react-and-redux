import React from "react";
import PropTypes from "prop-types";
import SelectInput from "../common/SelectInput";
import TextInput from "../common/TextInput";

const CourseForm = ({
  course,
  authors,
  onSave,
  onChange,
  saving = false,
  errors = {}
}) => {
  return (
    <div>
      <form onSubmit={onSave}>
        <h2>{course.id ? "Edit" : "Add"} Course</h2>
        {errors.onSave && (
          <div className="alert alert-danger" role="alert">
            {errors.onSave}
          </div>
        )}

        <TextInput
          name="title"
          label="Title"
          value={course.title && course.title.length > 0 ? course.title : ""}
          placeholder="Title of new course"
          onChange={onChange}
          error={errors.title}
        ></TextInput>

        <SelectInput
          name="authorId"
          label="Author"
          value={course.authorId || ""}
          defaultOption="Select author"
          options={authors.map(author => {
            return {
              value: author.id,
              text: author.name
            };
          })}
          onChange={onChange}
          error={errors.author}
        ></SelectInput>

        <TextInput
          name="category"
          label="Category"
          value={
            course.category && course.category.length > 0 ? course.category : ""
          }
          placeholder="Category of new course"
          onChange={onChange}
          error={errors.category}
        ></TextInput>

        <button type="submit" disabled={saving} className="btn btn-primary">
          {saving ? "Saving..." : "Save"}
        </button>
      </form>
    </div>
  );
};

CourseForm.propTypes = {
  course: PropTypes.object.isRequired,
  authors: PropTypes.array.isRequired,
  onSave: PropTypes.func.isRequired,
  onChange: PropTypes.func.isRequired,
  saving: PropTypes.bool,
  errors: PropTypes.object.isRequired
};

export default CourseForm;
